<?php


namespace App\Controller;


use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidUserAction
{
    public function __construct (EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }
    public function __invoke(Request $request,UserRepository $userRepository)
    {
      $tokenvalidation = $request->get('tokenValidation');
      $user = $userRepository->findOneBy(['tokenValidation'=>$tokenvalidation]);
      if ($user === null){
          return new JsonResponse(['error'=>'your token is not valid '],Response::HTTP_BAD_REQUEST);
      }
      if ($user->getIsEnable() === true){
          return new JsonResponse(['message'=>'your account is already validated'],Response::HTTP_OK);

      }
        if ($user->getTokenValidationExpireAt() <  new \DateTime() ){
            return new JsonResponse(['error'=>'your token is expired please retry'],Response::HTTP_BAD_REQUEST);

        }
        $user->setIsEnable(true);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return new JsonResponse(['success'=>'your account is now validated'],Response::HTTP_OK);
    }

}