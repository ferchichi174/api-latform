<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LoginAction extends AbstractController
{

    public function __invoke (Request $request){

    if (!$this->isGranted('IS_AUTHENTICATED_FULLY')){
        return new JsonResponse(['error'=>"Invalid login request:check that the content-type headers is 'application/json'." ],Response::HTTP_BAD_REQUEST);

    }
    $user = $this->getUser();
    if ($user->getIsEnable() === false ){

        return new JsonResponse(['error'=>"Your account is not validated" ],Response::HTTP_BAD_REQUEST);

    }
    return $this->getUser();
    }

}