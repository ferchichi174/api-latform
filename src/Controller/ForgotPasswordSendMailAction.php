<?php


namespace App\Controller;


use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ForgotPasswordSendMailAction extends AbstractController
{

    private $mailer;
    private $entityManager;
    public function __construct (EntityManagerInterface $entityManager, Mailer $mailer){
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function __invoke(Request $request , UserRepository $userRepository){

        $content = json_decode($request->getContent());
        $user = $userRepository->findOneBy(['email'=>$content->email]);
        if ($user !==Null){
            $subject ='forget password';
            $template = "emails/forgot-password.html.twig";
            $user->generateForgotToken();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->mailer->send($user,$subject,$template,['ForgotPasswordToken'=>$user->getForgotPasswordToken()]);

        }
        return new JsonResponse(['success'=>'the email has been successfull send'],Response::HTTP_OK);
    }

}