<?php


namespace App\Controller;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ForgotPasswordResetAction extends AbstractController
{
    private $userPasswordEncoder;
    private $validator;
    private $entityManager;
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder,ValidatorInterface $validator,EntityManagerInterface $entityManager){
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->validator = $validator;
        $this->entityManager = $entityManager;

    }
    public function __invoke(Request $request,UserRepository $userRepository){

        $forgetpasswortoken = $request->get('ForgotPasswordToken');
        $user = $userRepository->findOneBy(['ForgotPasswordToken' =>$forgetpasswortoken]);

        if ($user === null){

            return new JsonResponse(['error'=>'there is error '],Response::HTTP_BAD_REQUEST);

        }
        $content = json_decode($request->getContent());
        $password = null;
        if(isset($content->password)){
            $password= $content->password;
        }
        if (null === $password || empty($password)){
            return new JsonResponse(['error'=>'there is error in your password '],Response::HTTP_BAD_REQUEST);

        }
        if ($user->getForgotPasswordTokenExpireAt() < new \DateTime() ){
            return new JsonResponse(['error'=>'your token is expired'],Response::HTTP_BAD_REQUEST);

        }
        $user->setPlainPassword($password);
        $user->setPassword(
            $this->userPasswordEncoder->encodePassword($user,$user->getPlainPassword())
        );
        $this->validator->validate($user);
        $user->setForgotPasswordTokenExpireAt(Null);
        $user->setForgotPasswordToken(null);
        $user->eraseCredentials();
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return new JsonResponse(['success'=>'your password has been successfull updated'],Response::HTTP_OK);


    }

}