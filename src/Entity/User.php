<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\ValidUserAction;
use App\Controller\ValidUserResendMailAction;
use App\Controller\LoginAction;
use App\Controller\LogoutAction;
use App\Controller\ForgotPasswordSendMailAction;
use App\Controller\ForgotPasswordResetAction;
/**
 * @ApiResource(

 *     normalizationContext={"groups"= {"user:read"}},
 *     denormalizationContext={"groups"= {"user:write"}},
 *     collectionOperations={
 *     "get" = {
 *     "security" ="is_granted('ROLE_USER')"
 *     },
 *     "post"={
 *     "denormalization_context"={"groups"= {"user:item:post"}},
 *     "validation_Groups"={"create"}
 *
 *     },
 *     "login" = {
 *     "method" = "POST",
 *      "deserialize" = false,
 *      "path"= "/login",
 *      "controller" = LoginAction::class
 *
 *
 *
 *     },
 *         "logout" = {
 *     "method" = "POST",
 *      "deserialize" = false,
 *      "path"= "/logout",
 *      "controller" = LogoutAction::class
 *
 *
 *
 *     },
 *     "Validate" = {
 *      "method" = "PATCH",
 *      "deserialize" = false,
 *      "path"= "/users/{tokenValidation}/validate",
 *      "controller" = ValidUserAction::class
 *     },
 *      *     "resend-mail-validate" = {
 *      "method" = "POST",
 *      "deserialize" = false,
 *      "path"= "/users/resend-mail-validate",
 *      "controller" = ValidUserResendMailAction::class
 *     },
 *
 *          "forgot_password-reset" = {
 *      "method" = "PATCH",
 *      "deserialize" = false,
 *      "path"= "/forgot-password/{ForgotPasswordToken}",
 *      "controller" = ForgotPasswordResetAction::class
 *     },
 *      "forgot_password_send_mail" = {
 *      "method" = "POST",
 *      "deserialize" = false,
 *      "path"= "/forgot-password",
 *      "controller" = ForgotPasswordSendMailAction::class
 *     }
 *
 *     }
 );
 *     @UniqueEntity("email",message="this email already exist")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *@Groups({"user:item:post","user:read"})

     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(groups="create")
     * @Assert\NotBlank(groups="create")

     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")

     */
    private $password;

    /**
     * @SerializedName ("password")
     * @Groups("user:item:post")
     * @Assert\NotBlank(groups="create")
     * @Assert\Length(
     *     min = 8,
     *     max=32,
     *     minMessage="Your password must be at lead {{ limit }} caracter long",
     *     maxMessage="Your password cannot be longer then {{ limit }} caracter "
     * )
     */
    private $PlainPassword;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tokenValidation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $tokenValidationExpireAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ForgotPasswordToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ForgotPasswordTokenExpireAt;


    public function __construct()
    {
        $this->isEnable = false;
        $this->generateValidationToken();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
       $this->plainPassword = null;
    }

    public function getPlainPassword(): ?string
    {
        return $this->PlainPassword;
    }

    public function setPlainPassword(string $PlainPassword): self
    {
        $this->PlainPassword = $PlainPassword;

        return $this;
    }

    public function getIsEnable(): ?bool
    {
        return $this->isEnable;
    }

    public function setIsEnable(bool $isEnable): self
    {
        $this->isEnable = $isEnable;

        return $this;
    }

    public function getTokenValidation(): ?string
    {
        return $this->tokenValidation;
    }

    public function setTokenValidation(string $tokenValidation): self
    {
        $this->tokenValidation = $tokenValidation;

        return $this;
    }

    public function getTokenValidationExpireAt(): ?\DateTimeInterface
    {
        return $this->tokenValidationExpireAt;
    }

    public function setTokenValidationExpireAt(\DateTimeInterface $tokenValidationExpireAt): self
    {
        $this->tokenValidationExpireAt = $tokenValidationExpireAt;

        return $this;
    }
    public function generateValidationToken(){

        $expirationAt = new \DateTime('+1day');
        $token= rtrim(strtr(base64_encode(random_bytes(32)),'+/','-_'),'=');
        $this->setTokenValidation($token);
        $this->setTokenValidationExpireAt($expirationAt);
    }

    public function getForgotPasswordToken(): ?string
    {
        return $this->ForgotPasswordToken;
    }

    public function setForgotPasswordToken(?string $ForgotPasswordToken): self
    {
        $this->ForgotPasswordToken = $ForgotPasswordToken;

        return $this;
    }

    public function getForgotPasswordTokenExpireAt(): ?\DateTimeInterface
    {
        return $this->ForgotPasswordTokenExpireAt;
    }

    public function setForgotPasswordTokenExpireAt(?\DateTimeInterface $ForgotPasswordTokenExpireAt): self
    {
        $this->ForgotPasswordTokenExpireAt = $ForgotPasswordTokenExpireAt;

        return $this;
    }
    public function generateForgotToken(){

        $expirationAt = new \DateTime('+1day');
        $token= rtrim(strtr(base64_encode(random_bytes(32)),'+/','-_'),'=');
        $this->setForgotPasswordToken($token);
        $this->setForgotPasswordTokenExpireAt($expirationAt);
    }
}
