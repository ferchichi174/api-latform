<?php


namespace App\Controller;


use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ValidUserResendMailAction
{
    private $mailer;
    public function __construct (EntityManagerInterface $entityManager, Mailer $mailer){
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }
 public function __invoke(Request $request,UserRepository $userRepository){

    $content = json_decode($request->getContent() );

    $email = $content->email;
    $user = $userRepository->findOneBy(['email'=>$email]);
    if ($user === null){
        return new JsonResponse(['error'=>'there is error in your mail '],Response::HTTP_BAD_REQUEST);
    }
     if ($user->getIsEnable() === true){
         return new JsonResponse(['message'=>'your account is already validated'],Response::HTTP_OK);

     }
    $user->generateValidationToken();
     $this->entityManager->flush();
     $subject = "valid your account";
     $template = "email/user_validate.html.twig";
     $this->mailer->send($user,$subject,$template,['tokenValidation'=>$user->getTokenValidation()]);
     return new JsonResponse(['success'=>'the email has been successfull send'],Response::HTTP_OK);
 }
}