<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\User;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserDataPersister implements DataPersisterInterface
{
    private $entityManager;
    private $userPasswordEncoder;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager,UserPasswordEncoderInterface $userPasswordEncoder,Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->mailer = $mailer;
    }

    public function supports($data): bool
    {

        return $data instanceof User ;
    }
    /**
     * @param User $data
     */

    public function persist($data)
    {



        if ($data->getPlainPassword()){
            $data->setPassword(
                $this->userPasswordEncoder->encodePassword($data,$data->getPlainPassword())
            );
            $data->eraseCredentials();
        }
        if (null === $data->getId()){
        $subject = "Registration Mail";
        $template = "emails/registration.html.twig";
        $datasMail = ['tokenValidation' =>$data->getTokenValidation()];
        $this->mailer->send($data,$subject,$template,$datasMail);

        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }


    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }

}